import React from "react";
import Head from "next/head";
import HomeScreen from "@/components/HomeScreen";

const home = () => {
  return (
    <div>
      <Head>
        <title>Maid Simpl</title>
        <meta
          name="description"
          content="Nextly is a free landing page template built with next.js & Tailwind CSS"
        />
        <link rel="icon" href="/img/favicon.png" />
      </Head>
      <HomeScreen />
    </div>
  );
};

export default home;
