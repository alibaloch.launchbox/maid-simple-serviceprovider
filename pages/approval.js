import React, { useState } from "react";
import Image from "next/image";
import Head from "next/head";
import { motion } from "framer-motion";
import Navbar from "@/components/navbar";
import { useRouter } from "next/router";

const approval = () => {
  
  const router = useRouter();
  const variants = {
    hidden: { opacity: 0, x: -200, y: 0 },
    enter: { opacity: 1, x: 0, y: 0 },
    exit: { opacity: 0, x: 0, y: -100 },
  };

  const [toggle, setToggle] = useState(false);

  const handleContinue = () => {
    router.push("/home");
  };

  setTimeout(() => {
    setToggle(true);
  }, 2000);
  return (
    <>
      <Head>
        <title>Maid Simpl</title>
        <meta
          name="description"
          content="Nextly is a free landing page template built with next.js & Tailwind CSS"
        />
        <link rel="icon" href="/img/favicon.png" />
      </Head>
      <motion.div
        variants={variants} // Pass the variant object into Framer Motion
        initial="hidden" // Set the initial state to variants.hidden
        animate="enter" // Animated state to variants.enter
        exit="exit" // Exit state (used later) to variants.exit
        transition={{ type: "linear" }} // Set the transition to linear
        className=""
      >
        <Navbar />
        <div className="flex pt-20 h-[100vh]">
          <div className="w-1/2 bg-[#4a4a48] pb-44 pt-16">
            <span className="flex place-content-center mt-80">
              <Image
                src="/img/main-logo-white.png"
                alt="N"
                width="510"
                height="100"
              />
            </span>
          </div>
          <div className="w-1/2 bg-[#8cd790] pt-44">
            {!toggle ? (
              <p className="text-8xl tracking-widest text-center text-[#4a4a48] mt-48 font-bold">
                Please Wait <br /> for Admin <br /> Approval
              </p>
            ) : (
              <>
                <p className="text-7xl text-center text-[#4a4a48] mt-44 font-bold">
                  <span className="uppercase">Congratulations</span> ! <br />{" "}
                  Your Account has <br /> been Approved
                </p>
                <div className="flex place-content-center mt-12">
                  <motion.button
                    className="bg-black text-white px-14 py-5 rounded-lg text-xl"
                    onClick={() => handleContinue()}
                  >
                    Continue
                  </motion.button>
                </div>
              </>
            )}
          </div>
        </div>
      </motion.div>
    </>
  );
};

export default approval;
