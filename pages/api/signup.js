import DeleteUsers from "../../controllers.js/DeleteUser.js";
import getUsers from "../../controllers.js/getUsers.js";
import SignUp from "../../controllers.js/signup.js";
import connection from "../../lib/connection.js";

const signup = async (req, res) => {
  await connection();
  const method = req.method;
  switch (method) {
    case "POST":
      let result = await SignUp(req, res);
      break;
    case "GET":
      let getResult = await getUsers(req, res)
      break;
    case "DELETE":
      let deleteResult = await DeleteUsers(req, res)
      break;
  }
};

export default signup;
