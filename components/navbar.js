import Link from "next/link";
import Image from "next/image";
import { useState, useEffect } from "react";
import { motion } from "framer-motion";
import { useRouter } from "next/router";

const Navbar = () => {
  const router = useRouter();
  const token = router.query.token;

  const handleLogout = () => {
    router.push({
      pathname: "/home",
      query: {},
    });
  };
  const [isHovered, setIsHovered] = useState(false);
  
  const Login = (e) => {
    e.preventDefault()
    setTimeout(() => {

      router.push({pathname: "/login"});
    }, 500)
  };

  const handleFaq = () => {
    if (router.pathname === "/home") {
      const element = document.getElementById("faq");
      element.scrollIntoView({ behavior: "smooth" });
    } else {
      router.push({
        pathname: "/home",
        query: {
          token: token,
        },
      });

      setTimeout(() => {
        const element = document.getElementById("faq");
        element.scrollIntoView({ behavior: "smooth" });
      }, 1000);
    }
  };

  const handleAbout = () => {
    if (router.pathname === "/home") {
      const element = document.getElementById("about");
      element.scrollIntoView({ behavior: "smooth" });
    } else {
      router.push({
        pathname: "/home",
        query: {
          token: token,
        },
      });

      setTimeout(() => {
        const element = document.getElementById("about");
        element.scrollIntoView({ behavior: "smooth" });
      }, 1000);
    }
  };

  const handleContact = () => {
    if (router.pathname === "/home") {
      const element = document.getElementById("contact");
      element.scrollIntoView({ behavior: "smooth" });
    } else {
      router.push({
        pathname: "/home",
        query: {
          token: token,
        },
      });

      setTimeout(() => {
        const element = document.getElementById("contact");
        element.scrollIntoView({ behavior: "smooth" });
      }, 1000);
    }
  };

  const pathname = "window.location.pathname";

  return (
    <>
      <div
        className="fixed z-[1] w-full border-b-2 drop-shadow-2xl"
        style={{ backgroundColor: "#fcfbf7" }}
      >
        <motion.div
          className="container relative  flex flex-wrap items-end justify-between p-8 mx-auto lg:justify-between xl:px-0"
          animate={{
            y: [-250, 30, 0, 15, 0, 7, 0],
            transition: {
              duration: 3.0,
              times: [2.0, 2.2, 2.4, 2.6, 2.8, 3.0],
            },
          }}
        >
          <Link href="/">
            <span className="flex items-end space-x-2 text-2xl font-medium text-dark dark:text-gray-100">
              <span>
                <Image
                  src="/img/main-logo.png"
                  alt="N"
                  width="218"
                  height="90"
                  className="w-full"
                />
              </span>
            </span>
          </Link>
          {/* menu  */}
          <div className="hidden text-center lg:flex lg:items-end">
            <ul className="items-end justify-end flex-1 pt-6 list-none lg:pt-0 lg:flex">
              <li className="mr-3 nav__item">
                <Link
                  href={{
                    pathname: "/home",
                    query: {
                      token: token,
                    },
                  }}
                  className={
                    pathname === "/home"
                      ? "inline-block px-4 py-2 text-lg text-[#285943] font-bold no-underline rounded-md dark:text-gray-200 hover:text-[#77af9c]"
                      : "inline-block px-4 py-2 text-lg font-normal text-gray-800 no-underline rounded-md dark:text-gray-200 hover:text-[#77af9c]"
                  }
                >
                  HOME
                </Link>
              </li>
              <li className="mr-3 nav__item">
                <button
                  onClick={handleFaq}
                  className={
                    pathname === "/faq"
                      ? "inline-block px-4 py-2 text-lg text-[#285943] font-bold no-underline rounded-md dark:text-gray-200 hover:text-[#77af9c]"
                      : "inline-block px-4 py-2 text-lg font-normal text-gray-800 no-underline rounded-md dark:text-gray-200 hover:text-[#77af9c]"
                  }
                >
                  FAQ
                </button>
              </li>
              <li className="mr-3 nav__item">
                <button
                  onClick={handleAbout}
                  className={
                    "window.location.pathname" === "about"
                      ? "inline-block px-4 py-2 text-lg text-[#285943] font-bold no-underline rounded-md dark:text-gray-200 hover:text-[#77af9c]"
                      : "inline-block px-4 py-2 text-lg font-normal text-gray-800 no-underline rounded-md dark:text-gray-200 hover:text-[#77af9c]"
                  }
                >
                  ABOUT
                </button>
              </li>
              <li className="mr-3 nav__item">
                <Link
                  href={{
                    pathname: "/bookus",
                    query: {
                      token: token,
                    },
                  }}
                  className={
                    pathname === "/bookus"
                      ? "inline-block px-4 py-2 text-lg text-[#285943] font-bold no-underline rounded-md dark:text-gray-200 hover:text-[#77af9c]"
                      : "inline-block px-4 py-2 text-lg font-normal text-gray-800 no-underline rounded-md dark:text-gray-200 hover:text-[#77af9c]"
                  }
                >
                  BOOK US
                </Link>
              </li>
              <li className="mr-3 nav__item">
                <button
                  onClick={handleContact}
                  className={
                    pathname === "/contact"
                      ? "inline-block px-4 py-2 text-lg text-[#285943] font-bold no-underline rounded-md dark:text-gray-200 hover:text-[#77af9c]"
                      : "inline-block px-4 py-2 text-lg font-normal text-gray-800 no-underline rounded-md dark:text-gray-200 hover:text-[#77af9c]"
                  }
                >
                  CONTACT
                </button>
              </li>
            </ul>
          </div>
          <div className="hidden mr-3 space-x-4 lg:flex nav__item">
            {token ? (
              <div className="flex rounded-full bg-[#4a4a48] px-4 py-4 ">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  fill="white"
                  className="w-6 h-6"
                >
                  <path
                    fillRule="evenodd"
                    d="M7.5 6a4.5 4.5 0 119 0 4.5 4.5 0 01-9 0zM3.751 20.105a8.25 8.25 0 0116.498 0 .75.75 0 01-.437.695A18.683 18.683 0 0112 22.5c-2.786 0-5.433-.608-7.812-1.7a.75.75 0 01-.437-.695z"
                    clipRule="evenodd"
                  />
                </svg>
                <p
                  className="text-white font-semibold ml-2 cursor-pointer"
                  onClick={handleLogout}
                >
                  Logout
                </p>
              </div>
            ) : (
              <motion.button
                onClick={(e)=> Login(e)}
                className="px-8 py-3 text-white font-bold rounded-md md:ml-5"
                onMouseOver={() => setIsHovered(true)}
                onMouseLeave={() => setIsHovered(false)}
                style={{
                  backgroundColor: isHovered ? "#285943" : "#8cd790",
                  color: isHovered ? "white" : "#4a4a48",
                }}
                whileTap={{ scale: 0.7 }}
              >
                LOGIN
              </motion.button>
            )}
          </div>
        </motion.div>
      </div>
    </>
  );
};

export default Navbar;
