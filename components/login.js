import React, { useState, useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useSession, signIn, signOut, getProviders } from "next-auth/react";

const Login = () => {
  const router = useRouter();
  const { data: session, status } = useSession();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

//   const [facebook, setFacebook] = useState("");
//   const [google, setGoogle] = useState("");
  
//   const providers = async () => {
//     const prod = await getProviders();
//     console.log(prod)
//     setFacebook(prod.facebook);
//     setGoogle(prod.google);
//   };
//   providers()

  if (status === "authenticated") {
    signOut();
  }
  const handleGoogleSignin = (e) => {
    e.preventDefault();
    signIn(google.id);
  };

  const handleFacebookSignIn = (e) => {
    e.preventDefault();
    signIn(facebook.id)
  }
  const handleLogin = async (e) => {
    e.preventDefault();
    const data = {
      email: email,
      password: password,
    };

    const login = await fetch("/api/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    const response = await login.json();
    console.log(response);
    if (response.success === true) {
      router.push({
        pathname: "/home",
        query: {
          token: response.jwtToken,
        },
      });
    }
  };

  return (
    <div className="flex pt-36">
      <div className="w-1/2 pb-56 bg-[#4a4a48]">
        <span className="flex place-content-center mt-80">
          <Image
            src="/img/main-logo-white.png"
            alt="N"
            width="510"
            height="100"
          />
        </span>
      </div>
      <div className="w-1/2 bg-[#8cd790]">
        <p className="text-6xl text-center text-[#4a4a48] mt-28 font-bold">
          Hello Again !
        </p>
        <p className="text-center mt-8 text-xl">
          Welcome back, you've been missed.
        </p>

        <div className="flex place-content-center mt-9">
          <form>
            <input
              type="email"
              id="email"
              className="bg-gray-50 border border-gray-300 w-full text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Enter Email"
              required
              onChange={(e) => setEmail(e.target.value)}
              value={email}
            />
            <input
              type="password"
              id="\password]"
              className="bg-gray-50 mt-8 border border-gray-300 w-full text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Enter Password"
              required
              onChange={(e) => setPassword(e.target.value)}
              value={password}
            />
            <p className="flex place-content-end mt-3 text-gray-500">
              Forget Password?
            </p>
            <button
              className="w-full h-12 bg-[#4a4a48] mt-8 text-[#fcfbf7] font-bold rounded-md hover:bg-[#020617]"
              onClick={(e) => handleLogin(e)}
            >
              LOGIN
            </button>
            <p className="flex place-content-center mt-5 text-gray-500">
              Or continue with
            </p>
            <div className="flex place-content-center mt-6 ml-8">
              {!session && (
                <span
                  className="bg-[#fcfbf7] mr-8 pt-5 pl-4 pr-4 pb-2 rounded-md cursor-pointer"
                  onClick={handleFacebookSignIn}
                >
                  <Image
                    src="/img/facebook.png"
                    alt="N"
                    width="60"
                    height="60"
                  />
                </span>
              )}
              <span className="bg-[#fcfbf7] mr-8 pt-2 pl-4 pr-4 pb-2 rounded-md cursor-pointer" >
                <Image src="/img/apple.png" alt="N" width="60" height="60" />
              </span>
              <span className="bg-[#fcfbf7] mr-8 pt-4 pl-4 pr-4 pb-2 rounded-md cursor-pointer" onClick={handleGoogleSignin}>
                <Image src="/img/google.png" alt="N" width="60" height="60" />
              </span>
            </div>
          </form>
        </div>
        <p className="text-center text-gray-500 mt-24 mb-4">
          Not a member?{" "}
          <Link
            href="/signup"
            className="hover:underline hover:underline-offset-8"
          >
            Register now with Email
          </Link>
        </p>
      </div>
    </div>
  );
};

export default Login;
