import React from "react";
import Dropzone from "react-dropzone";

const MyAccount = () => {
  return (
    <>
      <div className="pt-64 bg-white pb-12">
        <p className="text-center text-5xl uppercase tracking-widest font-bold">
          My Account
        </p>
        <div className="flex place-content-center mt-10">
          <div className="bg-gray-200 w-5/6 rounded-xl pt-16 pb-10">
            <div className="flex ml-24 mr-24">
              <p className="text-2xl ml-6 text-black uppercase font-semibold w-5/6">
                Personal Information
              </p>
              <div className="flex place-content-end w-1/6 mr-6">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-6 h-6 cursor-pointer"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L6.832 19.82a4.5 4.5 0 01-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 011.13-1.897L16.863 4.487zm0 0L19.5 7.125"
                  />
                </svg>
              </div>
            </div>
            <div className="border border-1 border-black ml-28 mr-28 mt-2" />
            <div className="ml-32 mt-12">
              <div className="flex">
                <p className="text-xl font-bold mt-2 tracking-widest">EMAIL</p>
                <input
                  type="text"
                  id="name"
                  className="bg-gray-50 ml-11 border border-gray-300 w-full mr-32 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="Enter Email"
                  required
                />
              </div>
              <div className="flex mt-8">
                <p className="text-xl font-bold mt-2 tracking-widest">NAME</p>
                <div className="flex ">
                  <input
                    type="text"
                    id="name"
                    className="bg-gray-50 ml-12 border border-gray-300 w-[600px] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="First Name"
                    required
                  />
                  <input
                    type="text"
                    id="name"
                    className="bg-gray-50 ml-4 border border-gray-300 w-[600px] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="Last Name"
                    required
                  />
                </div>
              </div>
              <div className="flex mt-8">
                <p className="text-xl font-bold mt-2 tracking-widest">
                  CONTACT
                </p>
                <input
                  type="text"
                  id="name"
                  className="bg-gray-50 ml-2 border border-gray-300 w-full mr-32 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="Enter Email"
                  required
                />
              </div>
              <div className="flex mt-8">
                <p className="text-xl font-bold mt-5 tracking-widest">
                  PROFILE <br /> PICTURE
                </p>
                <div className="flex place-content-center border border-2 border-gray-500 ml-4 mr-12 rounded-lg w-[1220px]">
                  <Dropzone
                    onDrop={(acceptedFiles) => console.log(acceptedFiles)}
                  >
                    {({ getRootProps, getInputProps }) => (
                      <section>
                        <div {...getRootProps()} className="cursor-pointer">
                          <input {...getInputProps()} />
                          <div className="flex place-content-center pt-4">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              fill="none"
                              viewBox="0 0 24 24"
                              strokeWidth={1.5}
                              stroke="currentColor"
                              className="w-6 h-6"
                            >
                              <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                d="M15 11.25l-3-3m0 0l-3 3m3-3v7.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                              />
                            </svg>
                          </div>

                          <p className="text-center pt-2 pb-2">
                            Drag 'n' drop some files here, <br /> or click to
                            select files
                          </p>
                        </div>
                      </section>
                    )}
                  </Dropzone>
                </div>
              </div>
            </div>
            <div className="ml-60 mt-8 mr-32">
              <button className="bg-black text-white w-full rounded-md py-4 font-bold text-lg">SAVE</button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default MyAccount;
