import React, { useState, useEffect } from "react";
import Hero from "./hero";
import Navbar from "./navbar";
import Head from "next/head";
import Footer from "./footer";

const HomeScreen = () => {
  return (
    <>
      <Head>
        <title>Maid Simpl</title>
        <meta
          name="description"
          content="Nextly is a free landing page template built with next.js & Tailwind CSS"
        />
        <link rel="icon" href="/img/favicon.png" />
      </Head>
      <Navbar />
      <Hero />
      <Footer />
    </>
  );
};

export default HomeScreen;
