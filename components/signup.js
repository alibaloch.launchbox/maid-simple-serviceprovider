import React, { useState } from "react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import "react-responsive-modal/styles.css";
import { Modal } from "react-responsive-modal";

const Signup = () => {
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [confirm, setConfirm] = useState("");

  const [error, setError] = useState(false);
  const [passError, setPassError] = useState(false);
  const [modaltoggle, setModalToggle] = useState(false);

  //otp information
  const [otp, setOtp] = useState("");
  const [otp1, setOtp1] = useState("");
  const [otp2, setOtp2] = useState("");
  const [otp3, setOtp3] = useState("");
  const [otp4, setOtp4] = useState("");

  const onCloseModal = () => setModalToggle(false);

  const handleSubmit = async (e) => {
    router.push("/approval")
    // setLoading(true);
    // e.preventDefault();
    // const data = {
    //   name: name,
    //   email: email,
    //   password: pass,
    // };

    // if (
    //   data.name === "" ||
    //   data.email === "" ||
    //   data.pass === "" ||
    //   data.confirm === ""
    // ) {
    //   setError(true);
    //   setLoading(false);
    // } else {
    //   setError(false);
    //   if (data.password != confirm) {
    //     setPassError(true);
    //     setLoading(false);
    //   } else {
    //     const otp = Math.floor(1000 + Math.random() * 9000);
    //     const message = {
    //       email: email,
    //       message:
    //         "This OTP is Sent From MailBox, " +
    //         otp +
    //         " Do not share it with anyone",
    //     };
    //     const sendOTP = await fetch("/api/sendOtp", {
    //       method: "POST",
    //       headers: {
    //         "Content-Type": "application/json",
    //       },
    //       body: JSON.stringify(message),
    //     });
    //     const sendOTPResponse = await sendOTP.json();
    //     console.log(sendOTPResponse);
    //     if (sendOTPResponse.success == true) {
    //       setPassError(false);
    //       setLoading(false);
    //       setModalToggle(true);
    //       setOtp(otp);
    //     }
    //   }
    // }
  };

  const handleOtpCheck = async (e) => {
    e.preventDefault();
    setOTPLoading(true);
    const data = {
      name: name,
      email: email,
      password: pass,
    };

    const OTP = otp1 + otp2 + otp3 + otp4;
    if (OTP == parseInt(otp)) {
      console.log("matched");
      const getSign = await fetch("/api/signup", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      });
      const response = await getSign.json();
      if (response.result === "User Created Successfully") {
        router.push("/login");
      }
    }
  };

  return (
    <div className="flex pt-20">
      <div className="w-1/2 bg-[#4a4a48] pb-44 pt-16">
        <span className="flex place-content-center mt-80">
          <Image
            src="/img/main-logo-white.png"
            alt="N"
            width="510"
            height="100"
          />
        </span>
      </div>
      <div className="w-1/2 bg-[#8cd790]">
        <p className="text-6xl text-center text-[#4a4a48] mt-40 font-bold">
          Welcome !
        </p>
        <p className="text-center mt-6 mb-8 text-xl text-gray-500">
          Create an account, it's free.
        </p>

        <div className="flex place-content-center mt-10 w-full">
          <form>
            <input
              type="text"
              id="name"
              className="bg-gray-50 border border-gray-300 w-[540px] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Enter Full Name"
              required
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
            <input
              type="text"
              id="name"
              className="bg-gray-50 mt-2 border border-gray-300 w-[540px] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Preferred Name"
              required
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
            <input
              type="email"
              id="email"
              className="bg-gray-50 mt-2 border border-gray-300 w-[540px] text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Enter Email"
              required
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <div className="flex w-[540px]">
              <input
                type="password"
                id="password"
                className="bg-gray-50 mt-2 border border-gray-300 w-full text-gray-900 mr-2 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Enter Password"
                required
                value={pass}
                onChange={(e) => setPass(e.target.value)}
              />
              <input
                type="password"
                id="confirmPassword"
                className="bg-gray-50 mt-2 border border-gray-300 w-full text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Repeat Password"
                required
                value={confirm}
                onChange={(e) => setConfirm(e.target.value)}
              />
            </div>
            <div className="flex mt-2">
              <div className="w-1/2 bg-white mr-2 rounded-lg h-44">
                <div className="flex place-content-center mt-2 -ml-2">
                  <div className="bg-black rounded-full px-2 py-2 w-9 mt-2 ml-1">
                    <Image
                      src="/img/idcard.png"
                      alt="N"
                      width="25"
                      height="25"
                      className=""
                    />
                  </div>
                  <p className="mt-3 ml-2">Government ID</p>
                </div>
                <div className="ml-2 mr-2">
                  <p className="mt-3 text-center text-sm text-gray-500">
                    Take a Dirver's License
                  </p>
                  <p className="text-center text-sm text-gray-500">
                    National Identity Card or Passport Photo
                  </p>
                </div>
                <p className="text-center text-sm text-black underline underline-offset-4 mt-5 cursor-pointer">
                  Add Documents
                </p>
              </div>
              <div className="w-1/2 bg-white mr-2 rounded-lg h-44">
                <div className="flex place-content-center mt-2 -ml-2">
                  <div className="bg-black rounded-full px-2 py-2 w-9 mt-2 ml-1">
                    <Image
                      src="/img/idcard.png"
                      alt="N"
                      width="25"
                      height="25"
                      className=""
                    />
                  </div>
                  <p className="mt-3 ml-2">Government ID</p>
                </div>
                <div className="mr-2 ml-2">

                <p className="mt-3 text-center text-sm text-gray-500">
                  Take a Dirver's License
                </p>
                <p className="text-center text-sm text-gray-500">
                  National Identity Card or Passport Photo
                </p>
                </div>
                <p className="text-center text-sm text-black underline underline-offset-4 mt-5 cursor-pointer">
                  Add Documents
                </p>
              </div>
            </div>
            <button
              className="w-full h-12 bg-[#4a4a48] mt-8 text-[#fcfbf7] font-bold rounded-md hover:bg-[#020617]"
              onClick={(e) => handleSubmit(e)}
            >
              <div className="flex place-content-center">
                {loading ? (
                  <div role="status">
                    <svg
                      aria-hidden="true"
                      className="w-5 h-5  mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-black"
                      viewBox="0 0 100 101"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                        fill="currentColor"
                      />
                      <path
                        d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                        fill="currentFill"
                      />
                    </svg>
                    <span className="sr-only">Loading...</span>
                  </div>
                ) : (
                  <p> SIGN UP</p>
                )}
              </div>
            </button>
          </form>
          <Modal open={modaltoggle} onClose={onCloseModal} center>
            <div className="w-[588px]">
              <div className="flex place-content-center">
                <div className="">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-12 h-12"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M12 9v3.75m9-.75a9 9 0 11-18 0 9 9 0 0118 0zm-9 3.75h.008v.008H12v-.008z"
                    />
                  </svg>
                </div>
                <p className="ml-4 text-3xl uppercase font-semibold mt-1">
                  Confirm Your Email
                </p>
              </div>
              <div className="flex place-content-center">
                <input
                  type="email"
                  id="email"
                  className="bg-gray-50 mt-8 border border-gray-300 w-1/2 text-center text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="Enter Email"
                  required
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  disabled
                />
              </div>
              <div>
                <div className="w-full flex place-content-center">
                  <input
                    type="text"
                    id="text"
                    className="bg-gray-50 mr-10 mt-8 border border-gray-300 w-16 h-16 text-3xl font-bold text-center text-black rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder=""
                    required
                    maxLength={1}
                    value={otp1}
                    onChange={(e) => {
                      setOtp1(e.target.value);
                    }}
                  />
                  <input
                    type="text"
                    id="text"
                    className="bg-gray-50 mt-8 mr-10 border text-center border-gray-300 w-16 h-16 text-center text-black text-3xl font-bold rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder=""
                    required
                    maxLength={1}
                    value={otp2}
                    onChange={(e) => {
                      setOtp2(e.target.value);
                    }}
                  />
                  <input
                    type="text"
                    id="text"
                    className="bg-gray-50 mt-8 mr-10 border border-gray-300 w-16 h-16 text-center text-black text-3xl font-bold rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder=""
                    required
                    maxLength={1}
                    value={otp3}
                    onChange={(e) => {
                      setOtp3(e.target.value);
                    }}
                  />
                  <input
                    type="text"
                    id="text"
                    className="bg-gray-50 mt-8 border border-gray-300 w-16 h-16 text-center text-3xl font-bold text-black rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder=""
                    required
                    maxLength={1}
                    value={otp4}
                    onChange={(e) => {
                      setOtp4(e.target.value);
                    }}
                  />
                </div>
              </div>
              <div className="flex place-content-center">
                <button
                  className="w-1/2 w-96 h-12 bg-[#4a4a48] mt-8 text-[#fcfbf7] font-bold rounded-md hover:bg-[#020617]"
                  onClick={(e) => handleOtpCheck(e)}
                >
                  SUBMIT
                </button>
              </div>
              <div className="flex place-content-center">
                <button className="w-1/2 w-96 h-12 bg-white border border-2 border-black mt-8 text-black font-bold rounded-md hover:bg-black hover:text-white">
                  RESEND
                </button>
              </div>
            </div>
          </Modal>
        </div>
        {error ? (
          <div className="flex place-content-center">
            <p className="flex place-content-center text-center text-white bg-red-800 font-bold rounded-full py-1 mt-2 w-72">
              Please fill all the fields
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-5 h-6 ml-5 cursor-pointer"
                onClick={() => setError(false)}
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M6 18L18 6M6 6l12 12"
                />
              </svg>
            </p>
          </div>
        ) : (
          ""
        )}
        {passError ? (
          <div className="flex place-content-center">
            <p className="flex place-content-center text-center text-white bg-red-800 font-bold rounded-full py-1 mt-2 w-72">
              Password does not match
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-5 h-6 ml-5 cursor-pointer"
                onClick={() => setPassError(false)}
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M6 18L18 6M6 6l12 12"
                />
              </svg>
            </p>
          </div>
        ) : (
          ""
        )}
        <p className="text-center text-gray-500 mt-24 mb-24">
          Already a member?{" "}
          <Link
            href="/login"
            className="hover:underline hover:underline-offset-8"
          >
            Login
          </Link>
        </p>
      </div>
    </div>
  );
};

export default Signup;
