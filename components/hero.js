import { TypeAnimation } from "react-type-animation";

const Hero = () => {
  const text = "Welcome to Service Provider Section";

  return (
    <>
      <div className="pt-20 bg-white pb-36 h-[100vh]">
        <p className="text-center mt-80 pt-16 text-6xl font-semibold font-mono tracking-widest  ">
          <TypeAnimation
            sequence={[
              text,
              () => {
                console.log("Sequence completed"); // Place optional callbacks anywhere in the array
              },
            ]}
            wrapper="span"
            cursor={false}
          />
        </p>
      </div>
    </>
  );
};

export default Hero;
