import Link from "next/link";
import Image from "next/image";
import React from "react";
import { useRouter } from "next/router";

export default function Footer() {
  const router = useRouter();
  const token = router.query.token;

  const handleContact = () => {
    if (router.pathname === "/home") {
      const element = document.getElementById("contact");
      element.scrollIntoView({ behavior: "smooth" });
    } else {
      router.push({
        pathname: "/home",
        query: {
          token: token,
        },
      });
      setTimeout(() => {
        const element = document.getElementById("contact");
        element.scrollIntoView({ behavior: "smooth" });
      }, 1000);
    }
  };

  const handleAbout = () => {
    if (router.pathname === "/home") {
      const element = document.getElementById("about");
      element.scrollIntoView({ behavior: "smooth" });
    } else {
      router.push({
        pathname: "/home",
        query: {
          token: token,
        },
      });

      setTimeout(() => {
        const element = document.getElementById("about");
        element.scrollIntoView({ behavior: "smooth" });
      }, 1000);
    }
  };

  const handleBook = () => {
    router.push({pathname: "/bookus", query: {
      token: token
    }})
  }
  return (
    <>
      <div className="flex bg-[#285943] pt-12 pb-12">
        <div className="ml-36 pt-12 w-1/4 mr-36">
          <div className="flex place-content-center">
            <Image src="/img/blacklogo.png" alt="N" width="110" height="100" />
          </div>
          <p className="text-white text-center text-base mt-6">
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eligendi
            laboriosam hic exercitationem officiis eum consequatur error
            possimus nostrum modi delectus, corporis deserunt dolores illo
            distinctio eveniet ut eius explicabo quam!
          </p>
        </div>
        <div className="w-1/4 mt-24">
          <p className="text-white text-lg font-bold tracking-widest cursor-pointer hover:underline hover:underline-offset-4">
            Login
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          </p>
          <p className="text-white text-lg font-bold tracking-widest cursor-pointer hover:underline hover:underline-offset-4 mt-6" onClick={handleContact}>
            Contact Us &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          </p>
          <p className="text-white text-lg font-bold tracking-widest cursor-pointer hover:underline hover:underline-offset-4 mt-6"  onClick={handleAbout}>
            About
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          </p>
          <p className="text-white text-lg font-bold tracking-widest cursor-pointer hover:underline hover:underline-offset-4 mt-6" onClick={handleBook}>
            Book Now &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          </p>
        </div>
        <div className="w-1/4 mt-16">
          {" "}
          <Link
            href={{
              pathname: "/privacy",
              query: {
                token: token,
              },
            }}
          >
            <p className="text-white text-lg font-bold tracking-widest cursor-pointer hover:underline hover:underline-offset-4 mt-6">
              Privacy Policy
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </p>
          </Link>
          <Link
            href={{
              pathname: "/terms",
              query: {
                token: token,
              },
            }}
          >
            <p className="text-white text-lg font-bold tracking-widest cursor-pointer hover:underline hover:underline-offset-4 mt-6">
              Terms & Conditions &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </p>
          </Link>
        </div>
        <div className="w-1/4 mt-20 text-white font-semibold  ">
          <p className="ml-8">Address</p>
          <div className="flex place-content-start mt-4">
            <div className="mt-2">
              <Image src="/img/mapgray.png" alt="N" width="50" height="50" />
            </div>
            <p className="ml-4">
              Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sequi,
              perspiciatis possimus{" "}
            </p>
          </div>
          <div className="flex place-content-start mt-4">
            <div className="mt-2">
              <Image src="/img/phonegray.png" alt="N" width="35" height="35" />
            </div>
            <p className="ml-4 mt-3">202-555-0184</p>
          </div>
          <div className="flex place-content-start mt-4">
            <div className="mt-2">
              <Image src="/img/phonegray.png" alt="N" width="35" height="35" />
            </div>
            <p className="ml-4 mt-3">202-555-0184</p>
          </div>
        </div>
      </div>
    </>
  );
}
